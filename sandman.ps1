#powershell –ExecutionPolicy Bypass
#powershell Start-Process cmd -Verb runAs

##self-elevate the script if required
if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
 if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
  $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
  Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
  Exit
 }
}

#suppress standard error output
$ErrorActionPreference = "SilentlyContinue"

#echo "PlusOne Agent closing!"

##detecting and closing medadvisoragent.exe
$MedAdvisorAgent = Get-Process MedAdvisorAgent -ErrorAction SilentlyContinue
if ($MedAdvisorAgent) {
  # try gracefully first
  #echo "MedAdvisorAgent closing Gracefully"
  $MedAdvisorAgent.CloseMainWindow() | Out-Null
  # kill after five seconds
  Sleep 5
  if (!$MedAdvisorAgent.HasExited) {
    #echo "MedAdvisorAgent Force Shutdown"
    $MedAdvisorAgent | Stop-Process -Force
    Sleep 2
    #if program still doesn't close
    if (!$MedAdvisorAgent.HasExited) {
    taskkill /F /IM $MedAdvisorAgent
    }
  }
}
Remove-Variable MedAdvisorAgent

##closing the service gracefully
$MedAdvisorService = Get-Process MedAdvisorService -ErrorAction SilentlyContinue
if ($MedAdvisorService) {
  # try gracefully first
  #echo "MedAdvisorService closing Gracefully"
  (New-Object System.Net.WebClient).DownloadString("http://localhost:43222/api/Service/Quit")
  # kill after five seconds
  Sleep 5
  if (!$MedAdvisorService.HasExited) {
    #echo "MedAdvisorService Force Shutdown"
    $MedAdvisorService | Stop-Process -Force
    Sleep 2
    #if program still doesn't close
    if (!$MedAdvisorService.HasExited) {
    taskkill /F /IM $MedAdvisorService
    }
  }
}
Remove-Variable MedAdvisorService

##closing the medadvisor gracefully
$MedAdvisor = Get-Process MedAdvisor -ErrorAction SilentlyContinue
if ($MedAdvisor) {
  # try gracefully first
  #echo "MedAdvisor closing Gracefully"
  $MedAdvisor.CloseMainWindow() | Out-Null
  # kill after five seconds
  Sleep 5
  if (!$MedAdvisor.HasExited) {
    #echo "MedAdvisor Force Shutdown"
    $MedAdvisor | Stop-Process -Force
    Sleep 2
    #if program still doesn't close
    if (!$MedAdvisor.HasExited) {
    taskkill /F /IM $MedAdvisor
    }
  }
}
Remove-Variable MedAdvisor

#closing healthnotes* gracefully
$Healthnotes = Get-Process Healthnotes* -ErrorAction SilentlyContinue
if ($Healthnotes) {
  # try gracefully first
  #echo "Healthnotes closing Gracefully"
  $Healthnotes.CloseMainWindow() | Out-Null
  # kill after five seconds
  Sleep 5
  if (!$Healthnotes.HasExited) {
    #echo "Healthnotes Force Shutdown"
    $Healthnotes | Stop-Process -Force
    Sleep 2
    #if program still doesn't close
    if (!$Healthnotes.HasExited) {
    taskkill /F /IM $Healthnotes
    }
  }
}
Remove-Variable Healthnotes

echo "Done!!"
pause

#echo "PlusOne service shuting down"
#Start-Sleep -Seconds 15
#pause
#taskkill /F /IM "medadvisor.exe"
#taskkill /F /IM "healthnotes*"
#echo "Fin"
#pause